package be.kdg.snakesladders.presentation;

import static be.kdg.snakesladders.domain.Board.*;
import be.kdg.snakesladders.domain.Board;
import be.kdg.snakesladders.domain.Pawn;
import be.kdg.snakesladders.domain.Square;
import be.kdg.snakesladders.domain.Teleporter;

public class BoardDisplay {
  private final Board board;


  private static final int COLUMN_WIDTH = 6;
  private static final String COLUMN_SEPARATOR = "│";
  private static final String ROW_END = COLUMN_SEPARATOR + "\n";

  private static final String ROW_SEPARATOR = "─";
  private static final String TOP_BORDER_LEFT = "┌";
  private static final String TOP_BORDER_RIGHT = "┐";
  private static final String TOP_BORDER_MIDDLE = "┬";
  private static final String INNER_BORDER_LEFT = "├";
  private static final String INNER_BORDER_MIDDLE = "┼";
  private static final String INNER_BORDER_RIGHT = "┤";
  private static final String BOTTOM_BORDER_LEFT = "└";
  private static final String BOTTOM_BORDER_MIDDLE = "┴";
  private static final String BOTTOM_BORDER_RIGHT = "┘";

  public BoardDisplay(Board board) {
    this.board=board;
  }

  private void appendHorizontalBorder(StringBuilder sb, String left, String separator,
                                      String right) {
    sb.append(getHorizontalFieldBorder(left))
        .append(getHorizontalFieldBorder(separator).repeat(COLUMNS - 1))
        .append(right).append("\n");
  }

  private String getHorizontalFieldBorder(String separator) {
    return separator + ROW_SEPARATOR.repeat(COLUMN_WIDTH - 1);
  }

  private void appendTopBorder(StringBuilder sb) {
    appendHorizontalBorder(sb, TOP_BORDER_LEFT, TOP_BORDER_MIDDLE, TOP_BORDER_RIGHT);
  }

  private void appendInnerHorizontalBorder(StringBuilder sb) {
    appendHorizontalBorder(sb, INNER_BORDER_LEFT, INNER_BORDER_MIDDLE, INNER_BORDER_RIGHT);
  }

  private void appendBottomHorzontalBorder(StringBuilder sb) {
    appendHorizontalBorder(sb, BOTTOM_BORDER_LEFT, BOTTOM_BORDER_MIDDLE, BOTTOM_BORDER_RIGHT);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    appendTopBorder(sb);
    for (int row = ROWS - 1; row >= 0; row--) {
      appendPawnLine(sb, row);
      appendPositionLine(sb, row);
      appendSnakesLaddersLine(sb, row);
      if (row != 0) {
        appendInnerHorizontalBorder(sb);
      }
    }
    appendBottomHorzontalBorder(sb);
    return sb.toString();
  }

  private void appendPawnLine(StringBuilder sb, int row) {
    // The players' pawns
    for (int col = 0; col < COLUMNS; col++) {
      StringBuilder field = new StringBuilder(COLUMN_SEPARATOR).append(" ");
      Square square = board.getSquareAt(row,col);
      for (Pawn pawn: board.getPawns()) {
        if (pawn.getSquare().equals(square)) {
          field.append(pawn);
        }
      }
      sb.append(field).append(" ".repeat(COLUMN_WIDTH-field.length()));
    }
    sb.append(ROW_END);
  }

  private void appendSnakesLaddersLine(StringBuilder sb, int row) {
    // The Snake / Ladder heads/tails
    for (int col = 0; col < COLUMNS; col++) {
      sb.append(COLUMN_SEPARATOR).append(" ");
      Square square = board.getSquareAt(row,col);
      Teleporter teleporter = square.getTeleporter();
      if (teleporter == null) {
        sb.append("   ");
      } else if (teleporter.getFrom().equals(square)) {
        sb.append(teleporter.getFromString());
      } else {
        sb.append(teleporter.getToString());
      }
      sb.append(" ");
    }
    sb.append(ROW_END);
  }

  private void appendPositionLine(StringBuilder sb, int row) {
    // The position numbers
    for (int col = 0; col < COLUMNS; col++) {
      sb.append(COLUMN_SEPARATOR);
      sb.append(" ");
      Square square = board.getSquareAt(row,col);
      sb.append(String.format("%3d", square.getPosition()));
      sb.append(" ");
    }
    sb.append(ROW_END);
  }
}
