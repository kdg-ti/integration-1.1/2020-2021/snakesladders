package be.kdg.snakesladders.presentation;

public class Banners {
  // This class just contains some global text constants


  // Mind escaping "\" in the ASCII art!!
  public static final  String GOOD_LUCK =
    "  ____                 _   _            _    _ \n" +
    " / ___| ___   ___   __| | | |_   _  ___| | _| |\n" +
    "| |  _ / _ \\ / _ \\ / _` | | | | | |/ __| |/ / |\n" +
    "| |_| | (_) | (_) | (_| | | | |_| | (__|   <|_|\n" +
    " \\____|\\___/ \\___/ \\__,_| |_|\\__,_|\\___|_|\\_(_)\n";

  // easier with Java 15 text blocks
  // starts with empty line for readability, but that is not necessary
  public static final         String GOOD_GAME ="""    
  __ _  __ _
 / _` |/ _` |
| (_| | (_| |
 \\__, |\\__, |
 |___/ |___/
 """;


  public static final String WELCOME = """
 ____              _                ___
/ ___| _ __   __ _| | _____  ___   ( _ )
\\___ \\| '_ \\ / _` | |/ / _ \\/ __|  / _ \\/\\
 ___) | | | | (_| |   <  __/\\__ \\ | (_>  <
|____/|_| |_|\\__,_|_|\\_\\___||___/  \\___/\\/

 _              _     _
| |    __ _  __| | __| | ___ _ __ ___
| |   / _` |/ _` |/ _` |/ _ \\ '__/ __|
| |__| (_| | (_| | (_| |  __/ |  \\__ \\
|_____\\__,_|\\__,_|\\__,_|\\___|_|  |___/
""";

  public static final String HELP = """
       _   _      _
      | | | | ___| |_ __
      | |_| |/ _ \\ | '_ \\
      |  _  |  __/ | |_) |
      |_| |_|\\___|_| .__/
                   |_|
      Players take turns throwing a dice.
      They advance the number of squares they throw.
      The player that first reaches the end square wins.
      A square with an S contains a snake.
      If you land on a snake's head (example S2v)
      you slide back to the square with that snake's tail (example S2.).
      A square with an L contains a ladder.
      If you land on a ladder's start (example L3^)
      you climb up to the square with that ladder's end (example L3.).
      """;
}
