package be.kdg.snakesladders.domain;

public class Ladder extends Teleporter {
    public Ladder(int number, Square start, Square end) {
        super(number, start, end);
    }

    @Override
    public String getSymbol() {
        return "L";
    }

    @Override
    public String getStartString() {
        return "^";
    }

    @Override
    public String getEndString() {
        return ".";
    }




}
