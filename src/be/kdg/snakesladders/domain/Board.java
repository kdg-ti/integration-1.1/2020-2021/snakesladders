package be.kdg.snakesladders.domain;

import be.kdg.snakesladders.presentation.BoardDisplay;
import java.util.Random;


public class Board {
  public static final int ROWS = 8;
  public static final int COLUMNS = 8;
  public static final int SQUARES = ROWS * COLUMNS;
  private static final int SNAKE_COUNT = 5;
  private static final int LADDER_COUNT = 5;


  private final Random random = new Random();
  //private final Teleporter[] snakesAndLadders;
  private final Square[][] squares;
  private final Pawn[] pawns;
  private final BoardDisplay display;

  public Board(Pawn[] pawns) {
    this.squares = new Square[ROWS][COLUMNS];
    initializeSquares();
    generateRandomTeleporters();
    this.pawns = pawns;
    for (Pawn pawn : pawns) {
      pawn.setSquare(getSquareAt(1));
    }
    display = new BoardDisplay(this);
  }

  // rows and columns are 0 based!
  public static int getRow(int position) {
    return (position - 1) / COLUMNS;
  }

  public static int getColumn(int position) {
    int row = getRow(position);
    int evenColumn = position - row * COLUMNS - 1;
    // uneven columns are ordered left to right
    return row % 2 == 0 ? evenColumn : COLUMNS - evenColumn - 1;
  }

  public Pawn[] getPawns() {
    return pawns;
  }

  private void initializeSquares() {
    for (int position = 1; position <= SQUARES; position++) {
      Square square = new Square(position);
      squares[getRow(position)][getColumn(position)] = square;
    }
  }

  public Square getSquareAt(int position) {
    return squares[getRow(position)][getColumn(position)];
  }

  public Square getSquareAt(int row, int column) {
    return squares[row][column];
  }

  public Pawn getPawn(int index) {
    return pawns[index];
  }

  private void generateRandomTeleporters() {
    for (int i = 0; i < SNAKE_COUNT; i++) {
      generateRandomSnake(i + 1);
    }
    for (int i = 0; i < LADDER_COUNT; i++) {
      generateRandomLadder(i + 1);
    }
  }

  private Ladder generateRandomLadder(int id) {
    int start;
    Square startSquare;
    Square endSquare;
    do {
      // ladders should not start at 1 or last row
      start = random.nextInt(1, SQUARES - COLUMNS +1);
      startSquare = getSquareAt(start);
      // start from first square on next row
      // ladders should not end on last square
      endSquare =
          getSquareAt(random.nextInt((getRow(start) + 1) * COLUMNS + 1,
              SQUARES ));
      // if any square is occupied, try again
    } while (startSquare.hasTeleporter() || endSquare.hasTeleporter());
    return new Ladder(id, startSquare, endSquare);
  }

  private Snake generateRandomSnake(int id) {
    int start;
    Square startSquare;
    Square endSquare;
    do {
      // snakes should not start on first row or last square
      start = random.nextInt(COLUMNS + 1, SQUARES );
      startSquare = getSquareAt(start);
      // end on last square on previous row
      endSquare =
          getSquareAt(random.nextInt(1, getRow(start)  * COLUMNS +1));
      // if any square is occupied, try again
    } while (startSquare.hasTeleporter() || endSquare.hasTeleporter());
    return new Snake(id, startSquare, endSquare);
  }


  @Override
  public String toString() {
    return display.toString();
  }

  /**
   * Moves a pawn a certain amount of squares. Returns any teleporter used.
   *
   * @param player  The player that moves
   * @param squares The amount of squares to move.
   * @return Any teleporter used.
   */
  Turn movePawn(Player player, int squares, boolean replay) {
    Pawn pawn = player.getPawn();
    Square oldSquare = pawn.getSquare();
    int fromPosition = oldSquare.getPosition();
    int positionLanded = fromPosition + squares;
    if (positionLanded > SQUARES) {
      // player cannot move beyond the last square.
      positionLanded = SQUARES;
    }
    Square squareLanded = getSquareAt(positionLanded);
    Square squareTo = squareLanded.process();
    int toPosition = squareTo.getPosition();
    pawn.setSquare(squareTo);
    return new Turn(
        player.getName(),
        fromPosition,
        squares,
        toPosition,
        positionLanded < toPosition,
        positionLanded > toPosition,
        positionLanded,
        replay
    );
  }

}
