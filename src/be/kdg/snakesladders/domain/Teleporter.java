package be.kdg.snakesladders.domain;

public abstract class Teleporter {
  private final int id;
  private final Square from;
  private final Square to;

  public Teleporter(int number, Square from, Square to) {
    this.id = number;
    this.from = from;
    this.to = to;
    // bidirectional navigability
    from.setTeleporter(this);
    to.setTeleporter(this);
  }

  public Square getFrom() {
    return from;
  }

  public boolean startsFrom(Square square) {
    return from.equals(square);
  }

  public boolean endsAt(Square square) {
    return to.equals(square);
  }

  public Square getTo() {
    return to;
  }

  protected int getId() {
    return id;
  }

  public String getFromString() {
    return getSymbol() + getId() + getStartString();
  }

  public String getToString() {
    return getSymbol() + getId() + getEndString();
  }

  public abstract String getSymbol();

  public abstract String getStartString();

  public abstract String getEndString();


  public boolean startsOrEndsAt(Square square) {
    return startsFrom(square) || endsAt(square);
  }
}
