package be.kdg.snakesladders.domain;

public class Snake extends Teleporter {
    public Snake(int number, Square head, Square tail) {
        super(number, head, tail);
    }

    @Override
    public String getSymbol() {
        return "S";
    }

    @Override
    public String getStartString() {
        return "v";
    }

    @Override
    public String getEndString() {
        return ".";
    }


}
