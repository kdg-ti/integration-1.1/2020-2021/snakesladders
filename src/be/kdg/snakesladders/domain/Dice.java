package be.kdg.snakesladders.domain;

import java.util.Random;

public class Dice {
  private final int faces;
  private final Random random;

  public int getValue() {
    return value;
  }

  private int value;


  public Dice(int faces) {
    this.faces = faces;
    random=new Random();
    // initialising the dice
    roll();
  }

  public int roll(){
    value =random.nextInt(faces) + 1;
    return value;
  }
  
  
}
