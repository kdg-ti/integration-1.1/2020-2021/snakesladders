package be.kdg.snakesladders.domain;

import be.kdg.snakesladders.presentation.Banners;
import java.util.Scanner;

public class GameSession {
  public static final int MAX_PLAYERS = 4;
  public static final int DICE_FACES = 6;
  private static final Scanner scanner = new Scanner(System.in);
  private final Dice dice;
  private Board board;
  private Player[] players;
  private int currentPlayerIndex;
  private Player winner;
  private boolean active = true;

  public GameSession() {
    dice = new Dice(DICE_FACES);
    currentPlayerIndex = 0;
  }

  public void play() {
    start();
    playTurns();
    showEndGameScreen();
  }

  public void start() {
    System.out.println(Banners.WELCOME);
    players = createPlayers();
    board = createBoard(players);
    showScreenPrompt(buildStartGameBanner(players));
  }

  private Board createBoard(Player[] players) {
    Pawn[] pawns = new Pawn[players.length];
    for (int i = 0; i < players.length; i++) {
      pawns[i] = new Pawn(players[i].getName());
      players[i].setPawn(pawns[i]);
    }
    return new Board(pawns);
  }

  private String buildStartGameBanner(Player[] players) {
    StringBuilder builder = new StringBuilder(Banners.GOOD_LUCK)
        .append("\n")
        .append("Let's get started ");
    for (int i = 0; i < players.length; i++) {
      builder.append(players[i].getName());
      if (i < players.length - 2) {
        builder.append(", ");
      } else if (i == players.length - 2) {
        builder.append(" and ");
      }
    }
    builder.append("!\n")
        .append("\n");
    return builder.toString();
  }

  private Player[] createPlayers() {
    int nrOfPlayers = askPlayerCount();
    Player[] players = new Player[nrOfPlayers];
    for (int i = 0; i < nrOfPlayers; i++) {
      String playerName;
      do {
        System.out.printf("Player %d, please enter your name.%n", i + 1);
        System.out.print("> ");
        playerName = scanner.nextLine().strip();
      } while (playerName.isBlank());
      players[i] = new Player(playerName);
    }
    System.out.println();
    return players;
  }

  private int askPlayerCount() {
    int playerCount;
    System.out.print("Please enter the number of players> ");
    do {
      playerCount = scanner.nextInt();
      scanner.nextLine();
      if (playerCount < 1 || playerCount > MAX_PLAYERS) {
        System.err.printf("Please enter a number between %d and %d > ", 1, MAX_PLAYERS);
      } else {
        return playerCount;
      }
    } while (true);
  }

  public void playTurns() {
    drawBoard();
    do {
      processUserAction();
    } while (isSessionActive());
  }

  private void processUserAction() {
    switch (askPlayerInput(getCurrentPlayer())) {
      case "?", "h" -> showScreenPrompt(Banners.HELP);
      case "q" -> endSession();
      default -> playTurn();
    }
  }

  private void playTurn() {
    Turn summary = rollAndMove();
    drawBoard();
    System.out.println(summary);
  }

  private String askPlayerInput(Player player) {
    System.out.print(player.getName() + """
        , your turn:
        Press [ENTER] or 'r’ to roll the dice
        Press '?' for help
        Press ‘q’ to quit the game
        >\s""");
    String input = scanner.nextLine().strip().toLowerCase();
    System.out.println();
    return input;
  }

  public void drawBoard() {
    System.out.println(board.toString());
  }

  public Player getCurrentPlayer() {
    return players[currentPlayerIndex];
  }

  public boolean isSessionActive() {
    return active;
  }

  public Turn rollAndMove() {
    int fromPosition = getCurrentPlayer().getPosition();
    dice.roll();
    boolean replay = dice.getValue() == DICE_FACES;
    Player player = getCurrentPlayer();

    Turn summary = board.movePawn(player, dice.getValue(), replay);
    if (getCurrentPlayer().getPosition() == Board.SQUARES) {
      setWinner(getCurrentPlayer());
    } else if (!replay) {
      currentPlayerIndex = (currentPlayerIndex + 1) % players.length;
    }

    return summary;
  }

  private void setWinner(Player currentPlayer) {
    winner = currentPlayer;
    endSession();
  }

  private void endSession() {
    active = false;
  }


  private void showEndGameScreen() {
    if (winner != null) {
      System.out.println(Banners.GOOD_GAME);
      System.out.println();
      System.out.println("Congratulations, " + winner.getName() + "!");
    } else {
      System.out.println(getCurrentPlayer().getName() + " quit.");
      System.out.println("Better luck next time!");
    }
    System.out.println();
  }


  public void askPressAnyKey() {
    System.out.println("> Press enter to continue <");
    scanner.nextLine();
  }

  public void showScreenPrompt(String msg) {
    System.out.println(msg);
    askPressAnyKey();
  }
}
