package be.kdg.snakesladders.domain;

public class Pawn {
    private Square square;
    private final char display;

    public Pawn(char display) {
        this.display = display;
    }

    public Pawn(String name) {
        this.display = Character.toUpperCase(name.charAt(0));
    }

    public Pawn( char display,Square square) {
        this.square = square;
        this.display = display;
    }

    public Square getSquare() {
        return square;
    }

    public void setSquare(Square square) {
        this.square = square;
    }

    @Override
    public String toString() {
        return String.valueOf(display);
    }

    public int getPosition() {
        return square.getPosition();
    }
}
