package be.kdg.snakesladders.domain;

/**
 * A helper class to print out a turn summary
 */
public class Turn {
  private final String player;
  private final int numberRolled;
  private final int position;
  private final int fromPosition;
  private final int positionWithoutTeleport;
  private final boolean usedLadder;
  private final boolean bitBySnake;
  private final boolean replay;

  public Turn(String player, int fromPosition, int numberRolled, int position,
              boolean usedLadder, boolean bitBySnake, int positionWithoutTeleport, boolean replay) {
    this.player = player;
    this.fromPosition = fromPosition;
    this.numberRolled = numberRolled;
    this.position = position;
    this.usedLadder = usedLadder;
    this.bitBySnake = bitBySnake;
    this.positionWithoutTeleport = positionWithoutTeleport;
    this.replay=replay;
  }


  public String getPlayerName() {
    return player;
  }

  public int getNumberRolled() {
    return numberRolled;
  }

  public int getPosition() {
    return position;
  }

  public boolean hasUsedLadder() {
    return usedLadder;
  }

  public boolean wasBitBySnake() {
    return bitBySnake;
  }

  @Override
  public String toString() {
    StringBuilder whatHappened = new StringBuilder(String.format(
        "%s rolled a ‘%d’ and moved from square %d to %d%n",
        player,
        numberRolled,
        fromPosition,
        positionWithoutTeleport));
    if (usedLadder) {
      whatHappened.append(String.format("%s climbed a LADDER to square %d :)%n",
          player,
          position));
    } else if (bitBySnake) {
      whatHappened.append(String.format("%s was bitten by a SNAKE and fell to square %d :(%n",
          player,
          position));
    }
    if(replay){
      whatHappened.append(String.format("%s rolled a ‘%d’ and gets an extra turn!%n",
          player,
          numberRolled));
    }
    return whatHappened.toString();
  }
}

