package be.kdg.snakesladders.domain;

import java.util.Objects;

public class Square {
    /**
     * Positions are NOT zero-indexed! They go from 1 to 64 (in case of 8 x 8).
     */
    private final int position;
    private Teleporter teleporter;

    public Square(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Square square = (Square) o;
        return position == square.position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }

    public void setTeleporter(Teleporter teleporter) {
        this.teleporter = teleporter;

    }

    public Teleporter getTeleporter() {
        return teleporter;
    }

    public boolean hasTeleporter(){
        return teleporter != null;
    }

    public  boolean hasTeleporterStart(){
        return hasTeleporter() && this.equals(getTeleporter().getFrom());
    }

    public Square process(){
        Square transferred=this;
        if (hasTeleporterStart()){
            transferred= teleporter.getTo();
        }
        return transferred;
    }
}
