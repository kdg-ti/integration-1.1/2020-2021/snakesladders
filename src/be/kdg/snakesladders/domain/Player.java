package be.kdg.snakesladders.domain;

public class Player {
    private final String name;
    private  Pawn pawn;

    public Player(String name) {
        this.name = name;

    }

    public void setPawn(Pawn pawn) {
        this.pawn = pawn;
    }

    public String getName() {
        return name;
    }

    public Pawn getPawn() {
        return pawn;
    }

    @Override
    public String toString() {
        return getName();
    }

    public Square getSquare() {
        return getPawn().getSquare();
    }

    public int getPosition() {
        return pawn.getPosition();
    }
}
